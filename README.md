# 会议室预约

#### 介绍
基于SaaS服务开发的会议室预约系统，包含租户模块、权限模块、会议室模块、会议室预订模块、会议室资源和时间安排管理等功能； 使用技术栈有springboot+redis+mysql+tkmapper等

开发业务功能扩展是通过[叮叮智能saas运营平台](https://gitee.com/pianjiao006/pm)扩展开发

#### 软件架构
软件架构说明
- 后端架构基于SpringBoot2.0开发
- 权限控制：Spring Security和jwt
- 缓存：redis
- 数据库：mysql5.7
- 集成Mybatis通用Mapper: tk.mapper
- 前端：Vue


#### 安装教程

1.  扫码进入小程序
2.  点击预约
3.  选择会议室
4.  输入预约人信息，然后生成预约订单

小程序：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/114835_49275224_9226933.jpeg "ddqy-logo.jpg")

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 邀请有兴趣的前端小伙伴加入！！！
